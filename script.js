//1
//Класс зоряних війн
class StarWars {

    constructor() {
        this.allData = [];
        this.divsArray = [];
    }


//Метод для створення списків
    tagsCreator(array) {
        //Перебір обраного массивую створюю необхідну кількість списків
        array.forEach((element) => {
            //Створюю теги
            this.div = document.createElement('div');
            this.name = document.createElement('h1');
            this.crawl = document.createElement('p');
            this.ul = document.createElement('ul');
            
            //додаю теги
            document.body.appendChild(this.div);
            this.div.appendChild(this.name);
            this.div.appendChild(this.crawl);
            this.div.appendChild(this.ul);
            //Это походу ненадо(хай буде)
            this.div.classList.add(`${element.episodeId}`);
        });
    }


    //Метод для виклику запитів (впромісі щоб легше працювати з даними в інших методах)
    goRequest(url) {
        return new Promise((resolve) => {
            const request = new XMLHttpRequest();
            request.open('GET', url);
            request.send();
            request.onload = () => {
                resolve(JSON.parse(request.response));
            };
        })
    }


    //Метод для заповення списків (Виклчу останнім)
    tagsAdd(array) {
        //Массив з усіма списками на сторінці
        this.divsArray = document.querySelectorAll('div');
        //Перебор масиву фільмів
        array.forEach((element, index) => {
            //Деструктуризація списку що співпадає з цим фільмом
            let [h1,p,ul] = this.divsArray[index].children;
            //Заповнення текстових данних
            h1.textContent = element.name;
            p.textContent = element.openingCrawl;
            //Массив персонажів цього фільму
            let namesCharact = []

            //Масив промісів на основі масиву персонажів цього фільму який був перебраний з виконанням методу goRequest (щоб перевести всі послиння на персонажів в массиви) для кожного персонажа (КАПЕЦ)
            let promises = element.characters.map(pers => 
                this.goRequest(pers)
                //заповнення масиву імен персонажів з масиву інформаціі персонажів цього фільму
                .then(data => namesCharact.push(data.name))
            );
    
            //Проміс що спрацьовує після завершення всіх промісів в масиві промісвів 
            Promise.all(promises).then(() => {
                //Перебір масиву персонажів
                namesCharact.forEach(charName => {
                    //Додаваання елементу списку цього персонажа
                    let li = document.createElement('li');
                    li.textContent = charName;
                    ul.appendChild(li);
                });
            });
        });
    }
}


//Створення 
const HW = new StarWars();
//обробка первинних даних
HW.goRequest('https://ajax.test-danit.com/api/swapi/films')
    //заповнення масиву первиних даних
    .then(data =>data.forEach(el=>HW.allData.push(el)))
    //створення тегів і іх заповнення
    .then(() => {
        HW.tagsCreator(HW.allData);
        HW.tagsAdd(HW.allData)

    })